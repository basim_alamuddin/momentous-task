# Momentous Test (WiFinder & WiBot)
The project architecture is **MVVM** and makes use of **RxSwift** for binding between the ViewModel and the View.

### Technology used
* Xcode 10.1
* Swift 4.2
* [CocoaPods](https://cocoapods.org/)
* [RxSwift & RxCocoa](https://github.com/ReactiveX/RxSwift) - Reactive Programming in Swift
* [Swinject](https://github.com/Swinject/Swinject) - Dependency injection framework for Swift
* [Moya](https://github.com/Moya/Moya) - Network abstraction layer written in Swift
* [R.swift](https://github.com/mac-cain13/R.swift) - For strong typed, autocompleted resources like images, fonts and segues in Swift projects
* [SwiftLint](https://github.com/realm/SwiftLint) - A tool to enforce Swift style and conventions
* [Kingfisher](https://github.com/onevcat/Kingfisher) - A lightweight, pure-Swift library for downloading and caching images from the web

### Notes
* Commit messages follow the [Udacity Git Commit Message Style Guide](https://udacity.github.io/git-styleguide/).
* Pods are not checked into the repository so `pod install --repo-update` is required.
* Using (multiple) Storyboards just as glorified XIBs.

### Author

- Basim Alamuddin - basim.alamuddin@gmail.com