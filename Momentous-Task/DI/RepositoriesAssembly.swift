//
//  RepositoriesAssembly.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit
import Swinject
import Moya

/// Swinject assembly for repositories.
class RepositoriesAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(MediaRepository.self) { r in
            return MediaRepository(api: r.resolve(MoyaProvider<MediaAPI>.self)!)
        }
    }
}
