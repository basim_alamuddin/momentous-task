//
//  StoryboardsAssembly.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit
import Swinject

/// Swinject assembly for storyboards.
class StoryboardsAssembly: Assembly {
    
    func assemble(container: Container) {
        
        container.register(UIStoryboard.self, name: R.storyboard.wiFinder.name) { _ in
            R.storyboard.wiFinder()
        }
        
        container.register(UIStoryboard.self, name: R.storyboard.wiBot.name) { _ in
            R.storyboard.wiBot()
        }
    }
}
