//
//  ViewModelsAssembly.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit
import Swinject

/// Swinject assembly for view models.
class ViewModelsAssembly: Assembly {
    
    func assemble(container: Container) {

        container.register(WiFinderViewModel.self) { r in
            return WiFinderViewModel(repository: r.resolve(MediaRepository.self)!)
        }
        
        container.register(WiBotViewModel.self) { _ in
            return WiBotViewModel()
        }
    }
}
