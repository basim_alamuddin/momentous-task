//
//  APIAssembly.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit
import Swinject
import Moya

/// Swinject assembly for Moya APIs.
class APIAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(MoyaProvider<MediaAPI>.self) { _ in
            if isDebug {
                return MoyaProvider<MediaAPI>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: moyaJSONResponseDataFormatter)])
            } else {
                return MoyaProvider<MediaAPI>()
            }
        }
    }
}

/// JSON formatter function for NetworkLoggerPlugin
private func moyaJSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}
