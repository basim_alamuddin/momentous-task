//
//  DIWrapper.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit
import Swinject

/// Dependency Injection wrapper class that composes Swinject assembler.
public class DIWrapper {

    private let assembler = Assembler()
    public var resolver: Resolver {
        return assembler.resolver
    }
    
    init() {
//        Container.loggingFunction = nil  // Disable Swinject logs.
        
        assembler.apply(assemblies: [StoryboardsAssembly(),
                                     ViewControllersAssembly(),
                                     ViewModelsAssembly(),
                                     APIAssembly(),
                                     RepositoriesAssembly()])
        
    }
}
