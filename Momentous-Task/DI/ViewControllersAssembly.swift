//
//  ViewControllersAssembly.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit
import Swinject

/// Swinject assembly for view controllers.
class ViewControllersAssembly: Assembly {
    
    // swiftlint:disable force_cast
    func assemble(container: Container) {
        
        container.register(WiFinderViewController.self) { r in
            let storyboard = r.resolve(UIStoryboard.self, name: R.storyboard.wiFinder.name)!
            return storyboard.instantiateViewController(withIdentifier: R.storyboard.wiFinder.wiFinderViewController.identifier) as! WiFinderViewController
        }.initCompleted { r, vc in
            let vm = r.resolve(WiFinderViewModel.self)!
            vc.viewModel = vm
        }
        
        container.register(WiBotViewController.self) { r in
            let storyboard = r.resolve(UIStoryboard.self, name: R.storyboard.wiBot.name)!
            return storyboard.instantiateViewController(withIdentifier: R.storyboard.wiBot.wiBotViewController.identifier) as! WiBotViewController
        }.initCompleted { r, vc in
            let vm = r.resolve(WiBotViewModel.self)!
            vc.viewModel = vm
        }
        
        
    }
}
