//
//  MediaCellProtocol.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/19/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

protocol MediaCell: class {
    func setup(with media: Media)
}
