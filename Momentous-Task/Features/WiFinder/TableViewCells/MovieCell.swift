//
//  MovieCell.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/19/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher

class MovieCell: UITableViewCell, MediaCell {
    
    @IBOutlet weak var artworkImage: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var longDescription: UILabel!
    
    let disposeBag = DisposeBag()
    let tapGesture = UITapGestureRecognizer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // shake the artwork image when tapping it.
        tapGesture.rx.event.bind { [weak self] _ in
            self?.artworkImage.shake()
        }.disposed(by: disposeBag)
        
        artworkImage.addGestureRecognizer(tapGesture)
    }
    
    func setup(with media: Media) {
        artworkImage.kf.setImage(with: media.artworkUrl100, options: [.transition(.fade(0.2))])
        trackName.text = media.trackName
        longDescription.text = media.longDescription
    }
}
