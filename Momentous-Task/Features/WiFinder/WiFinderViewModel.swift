//
//  WiFinderViewModel.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class WiFinderViewModel {
    
    /// Screen title.
    let title = "WiFinder"
    let searchPlaceholder = "Search Media"
    private let mediaTypes: [MediaType] = [.music, .tvShow, .movie]
    /// SearchBar scopeButtonTitles.
    var mediaTypesForUI: [String] {
        return mediaTypes.map { $0.uiValue }
    }
    
    private let disposeBag = DisposeBag()
    private let repository: MediaRepository
    let mediaArray = BehaviorRelay(value: [Media]())
    /// Is requesting media or not.
    let isRequestingMedia = BehaviorRelay(value: false)
    /// Empty search result or not.
    let noMediaFound = BehaviorRelay(value: false)
    let errorHappened = PublishRelay<String>()
    let previewMedia = PublishRelay<(URL, String)>() // (media url, media title) tuple.
    
    
    init(repository: MediaRepository) {
        self.repository = repository
    }
    
    
    /// Search for media.
    ///
    /// - Parameters:
    ///   - term: Text string you want to search for.
    ///   - mediaIndex: The selected media type by the search bar scope buttons.
    func requestMedia(term: String, mediaIndex: Int) {
        let trimmedTerm = term.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if trimmedTerm.isEmpty {
            return
        }
        
        isRequestingMedia.accept(true)
        noMediaFound.accept(false)
        mediaArray.accept([]) // Clear the table view from previous search.
        
        repository.requestMedia(term: trimmedTerm, mediaType: mediaTypes[mediaIndex])
            .subscribe(onSuccess: { [weak self] in
                guard let self = self else { return }
                self.isRequestingMedia.accept(false)
                self.mediaArray.accept($0)
                if $0.isEmpty { self.noMediaFound.accept(true) }
            }, onError: { [weak self] error in
                guard let self = self else { return }
                Log.print(error)
                self.isRequestingMedia.accept(false)
                self.errorHappened.accept(error.localizedDescription)
            }).disposed(by: disposeBag)
    }
    
    func cellIdentifierFor(mediaType: MediaType) -> String {
        switch mediaType {
        case .music:
            return R.reuseIdentifier.music_cell.identifier
        case .tvShow:
            return R.reuseIdentifier.tv_show_cell.identifier
        case .movie:
            return R.reuseIdentifier.movie_cell.identifier
        }
    }
    
    /// Called when a user selects media item in the table view.
    func userTappedOn(media: Media) {
        if let previewUrl = media.previewUrl {
            previewMedia.accept((previewUrl, media.title))
            Log.print("Clicked media url: \(previewUrl)")
        } else {
            errorHappened.accept("The selected media doesn't contain a preview sample.")
        }
    }
    
}
