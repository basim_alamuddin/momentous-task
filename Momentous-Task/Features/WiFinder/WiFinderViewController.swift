//
//  WiFinderViewController.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AVFoundation
import AVKit

class WiFinderViewController: UIViewController {
    
    @IBOutlet weak var mediaTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var noMediaFound: UILabel!
    
    var viewModel: WiFinderViewModel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupViewModelBinding()
        handleWhenNavigating()
        respondToKeyboardNotifications()
    }
    
    private func setupUI() {
        title = viewModel.title
        tabBarItem.image = R.image.wifinder_tab()
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = viewModel.searchPlaceholder
        searchController.searchBar.scopeButtonTitles = viewModel.mediaTypesForUI
        searchController.searchBar.delegate = self
        
        navigationItem.searchController = searchController
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.hidesSearchBarWhenScrolling = false
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.view.backgroundColor = .white
        definesPresentationContext = true
    }
    
    /// Setup binding with view model reactive properties.
    private func setupViewModelBinding() {
        
        viewModel.isRequestingMedia.bind(to: activityIndicator.rx.isAnimating)
            .disposed(by: disposeBag)
        
        viewModel.noMediaFound.map { !$0 }
            .bind(to: noMediaFound.rx.isHidden)
            .disposed(by: disposeBag)
        
        // Bind media array with the table view items.
        viewModel.mediaArray.bind(to: mediaTableView.rx.items) { [unowned self] (tableView, _, media) in
            let cellId = self.viewModel.cellIdentifierFor(mediaType: media.kind)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId)!
            // swiftlint:disable force_cast
            (cell as! MediaCell).setup(with: media)
            // swiftlint:enable force_cast
            return cell
        }.disposed(by: disposeBag)
        
        viewModel.errorHappened.subscribe(onNext: { [weak self] errorMessage in
            self?.showAlert(title: "Error", message: errorMessage)
        }).disposed(by: disposeBag)
        
        // When an item is selected preview it.
        mediaTableView.rx.modelSelected(Media.self)
            .subscribe(onNext: { [weak self] mediaItem in
                self?.viewModel.userTappedOn(media: mediaItem)
            }).disposed(by: disposeBag)
        
        viewModel.previewMedia.subscribe(onNext: { [weak self] tuple in
            self?.playMedia(url: tuple.0, title: tuple.1)
        }).disposed(by: disposeBag)
    }
    
    private func playMedia(url: URL, title: String) {
        let playerViewController = AVPlayerViewController()
        playerViewController.title = title
        playerViewController.view.backgroundColor = .white
        
        let player = AVPlayer(url: url)
        playerViewController.player = player
        
        navigationController?.pushViewController(playerViewController, animated: true)
    }
    
    private func handleWhenNavigating () {
        
        navigationController?.rx.willShow.subscribe(onNext: { vc, _ in
            // Set large titles to false when pushing AVPlayerViewController
            if let playerViewController = vc as? AVPlayerViewController {
                playerViewController.navigationController?.navigationBar.prefersLargeTitles = false
                playerViewController.player!.play()
            }
        }).disposed(by: disposeBag)
        
        navigationController?.rx.didShow.subscribe(onNext: { vc, _ in
            if let wiFinderViewController = vc as? WiFinderViewController {
                // Restore large title to true after popping AVPlayerViewController
                wiFinderViewController.navigationController?.navigationBar.prefersLargeTitles = true
                
                // Deselect selected cell (if found) after popping AVPlayerViewController.
                if let selectedIndex = wiFinderViewController.mediaTableView.indexPathForSelectedRow {
                    wiFinderViewController.mediaTableView.deselectRow(at: selectedIndex, animated: true)
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func respondToKeyboardNotifications() {
        
        NotificationCenter.default.rx.notification(UIResponder.keyboardDidShowNotification)
        .subscribe(onNext: { [weak self] notification in
            if let keyboardHeight = notification.keyboardSize?.height {
                let safeAreaBottomInset = self?.view.safeAreaInsets.bottom ?? 0
                self?.mediaTableView.contentInset.bottom = keyboardHeight - safeAreaBottomInset
                self?.mediaTableView.scrollIndicatorInsets.bottom = keyboardHeight - safeAreaBottomInset
            }
        }).disposed(by: disposeBag)
        
        NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification)
        .subscribe(onNext: { [weak self] _ in
            self?.mediaTableView.contentInset.bottom = 0
            self?.mediaTableView.scrollIndicatorInsets.bottom = 0
        }).disposed(by: disposeBag)
    }

}

extension WiFinderViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {}
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
        viewModel.requestMedia(term: searchBar.text ?? "", mediaIndex: searchBar.selectedScopeButtonIndex)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        viewModel.requestMedia(term: searchBar.text ?? "", mediaIndex: searchBar.selectedScopeButtonIndex)
    }
}
