//
//  BotMessageCell.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/21/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit

class BotMessageCell: UITableViewCell, MessageCell {

    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var textBackground: UIView!
    @IBOutlet weak var messageDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        textBackground.layer.cornerRadius = 8
    }
    
    func bind(with viewModel: MessageViewModel) {
        messageText.text = viewModel.message.text
        messageDate.text = viewModel.formattedDate
    }
}
