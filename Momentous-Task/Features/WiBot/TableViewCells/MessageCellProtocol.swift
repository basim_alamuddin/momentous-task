//
//  MessageCellProtocol.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/21/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import Foundation

protocol MessageCell: class {
    func bind(with viewModel: MessageViewModel)
}
