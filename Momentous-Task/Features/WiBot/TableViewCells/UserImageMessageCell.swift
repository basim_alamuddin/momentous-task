//
//  UserImageMessageCell.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/21/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit

class UserImageMessageCell: UITableViewCell, MessageCell {

    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var messageDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        messageImage.layer.cornerRadius = 16
    }
    
    func bind(with viewModel: MessageViewModel) {
        messageImage.image = viewModel.message.image
        messageDate.text = viewModel.formattedDate
    }
    
}
