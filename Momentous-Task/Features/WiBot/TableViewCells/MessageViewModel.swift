//
//  MessageViewModel.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/21/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import Foundation

class MessageViewModel {
    
    let message: Message
    var formattedDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        return dateFormatter.string(from: message.date)
    }
    
    init(message: Message) {
        self.message = message
    }
}
