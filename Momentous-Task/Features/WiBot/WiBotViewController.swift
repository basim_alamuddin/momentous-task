//
//  WiBotViewController.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/20/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class WiBotViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var messagesTableView: UITableView!
    @IBOutlet weak var textInput: UITextView!
    @IBOutlet weak var pickImage: UIButton!
    @IBOutlet weak var sendText: UIButton!
    /// Bottom layout constraint of text view.
    @IBOutlet weak var inputBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    
    var viewModel: WiBotViewModel!
    let disposeBag = DisposeBag()
    // The initial space between input text view and screen bottom.
    var inputInitialBottomMargin: CGFloat!
    
    // MARK: - Overridden methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupViewModelBinding()
        respondToKeyboardNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.sendBotWelcomeMessage()
        
        // Fix: set input area to its default state after dismissing the image picker. If
        //      keyboard was open when presenting the image picker
        inputBottomConstraint.constant = inputInitialBottomMargin
    }
    
    // MARK: - Setup
    
    func setupUI() {
        inputInitialBottomMargin = inputBottomConstraint.constant
        title = viewModel.title
        tabBarItem.image = R.image.bot_tab()
        
        // Set table view display data from bottom to top.
        messagesTableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        // Text View Styling
        textInput.layer.cornerRadius = textInput.frame.height / 2
        textInput.layer.borderWidth = 1
        textInput.layer.borderColor = UIColor.gray.cgColor
        textInput.contentInset.left = 5
        textInput.contentInset.right = 5
        
        // Send text button is only enabled if there is text to send.
        sendText.isEnabled = false
        textInput.rx.didChange
            .map { [weak self] in
                let text = self?.textInput.text ?? ""
                self?.adjustTextInputHeight()
                return !text.isEmpty
            }.bind(to: sendText.rx.isEnabled).disposed(by: disposeBag)
        
        sendText.rx.tap.subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.messagesTableView.setContentOffset(.zero, animated: false)
            self.viewModel.sendUserMessage(with: self.textInput.text)
            self.textInput.text = ""
            self.sendText.isEnabled = false
            self.scrollToLastMessage()
        }).disposed(by: disposeBag)
        
        pickImage.rx.tap.subscribe(onNext: { [weak self] in
            self?.openImagePicker()
        }).disposed(by: disposeBag)
    }
    
    func setupViewModelBinding() {
        
        viewModel.messages.bind(to: messagesTableView.rx.items) { [unowned self] (tableView, _, message) in
            let cellId = self.viewModel.cellIdentifierFor(message: message)
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId)!
            // swiftlint:disable force_cast
            (cell as! MessageCell).bind(with: MessageViewModel(message: message))
            // swiftlint:enable force_cast
            // Flip the cell to appear correctly (the table view is already flipped).
            cell.transform = tableView.transform
            return cell
        }.disposed(by: disposeBag)
    }
    
    // MARK: - Helper methods
    
    func respondToKeyboardNotifications() {
        
        NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification)
            .subscribe(onNext: { [weak self] notification in
                if let keyboardHeight = notification.keyboardSize?.height, let animationDuration = notification.keyboardAnimationDuration {
                    self?.animateInputArea(by: keyboardHeight, animationDuration: animationDuration, keyboardIsShowing: true)
                }
            }).disposed(by: disposeBag)
        
        NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification)
            .subscribe(onNext: { [weak self] notification in
                if let animationDuration = notification.keyboardAnimationDuration {
                    self?.animateInputArea(by: 0, animationDuration: animationDuration, keyboardIsShowing: false)
                }
            }).disposed(by: disposeBag)
    }
    
    func animateInputArea(by keyboardHeight: CGFloat, animationDuration: Double, keyboardIsShowing: Bool) {
        let safeAreaBottomInsetAddition = view.safeAreaInsets.bottom * (keyboardIsShowing ? -1 : 0)
        UIView.animate(withDuration: animationDuration, animations: {
            self.inputBottomConstraint.constant = self.inputInitialBottomMargin + keyboardHeight + safeAreaBottomInsetAddition
            self.view.layoutIfNeeded()
        })
    }
    
    func adjustTextInputHeight() {
        if textInput.numberOfLines() > 10 {
            textInput.isScrollEnabled = true
        } else {
            textInput.isScrollEnabled = false
        }
    }
    
    func openImagePicker() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        self.present(imagePickerController, animated: true)
    }
    
    func scrollToLastMessage() {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.messagesTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
}

// MARK: - Extensions

extension WiBotViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        picker.dismiss(animated: true) {
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                self.viewModel.sendUserMessage(with: image)
                self.scrollToLastMessage()
            }
        }
    }
}
