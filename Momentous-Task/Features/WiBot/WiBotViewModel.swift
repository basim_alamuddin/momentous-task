//
//  WiBotViewModel.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/20/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class WiBotViewModel {
    
    let title = "WiBot"
    let messages = BehaviorRelay(value: [Message]())
    
    func cellIdentifierFor(message: Message) -> String {
        switch message.type {
        case .text:
            switch message.owner {
            case .user:
                return R.reuseIdentifier.user_text_message_cell.identifier
            case .bot:
                return R.reuseIdentifier.bot_message_cell.identifier
            }
        case .image:
            return R.reuseIdentifier.user_image_message_cell.identifier
        }
    }
    
    func sendUserMessage(with text: String) {
        let textToSend = text.trimmingCharacters(in: .whitespacesAndNewlines)
        if textToSend.isEmpty { return }
        let message = Message(owner: .user, text: textToSend)
        appendNew(message: message)
        
        respondToUserMessage()
    }
    
    func sendUserMessage(with image: UIImage) {
        let message = Message(owner: .user, image: image)
        appendNew(message: message)
        
        respondToUserMessage()
    }
    
    func sendBotMessage(with text: String) {
        let message = Message(owner: .bot, text: text)
        appendNew(message: message)
    }
    
    private func appendNew(message: Message) {
        var tempMessages = messages.value
        tempMessages.insert(message, at: 0)  // at: 0 because table view is flipped vertically.
        messages.accept(tempMessages)
    }
    
    func sendBotWelcomeMessage() {
        if messages.value.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.sendBotMessage(with: "Hi, you can send me text or image messages! 😉")
            }
        }
    }
    
    private func respondToUserMessage() {
        var responseText: String!
        
        if let lastUserMessage = messages.value.first { // .first because table view is flipped vertically.
            switch lastUserMessage.type {
            case .text:
                responseText = "Text message 🎉.\nUnfortunately I don't have enough AI to process it now!"
            case .image:
                responseText = "Image message 🎉.\nUnfortunately I don't have enough AI to process it now!"
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.sendBotMessage(with: responseText)
        }
    }
}
