//
//  AppDelegate.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/17/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let di = DIWrapper() // Dependency Injection Wrapper.

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupWindow()
        
        return true
    }
    
    /// Initialize window rootViewController.
    private func setupWindow() {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        self.window = window
        window.backgroundColor = UIColor.black
        
        // Add both WiFinderViewController and WiBotViewController in two separate UINavigationController then add the two UINavigationController in a UITabBarController
        let viewControllers = [AppDelegate.di.resolver.resolve(WiFinderViewController.self)!,
                           AppDelegate.di.resolver.resolve(WiBotViewController.self)!]
        // Force WiBotViewController to display its title in tab bar
        _ = viewControllers[1].view
        // Force showing tab bar image without calling .view here
        viewControllers[0].tabBarItem.image = R.image.wifinder_tab() //
        let tabBarVC = UITabBarController()
        tabBarVC.viewControllers = viewControllers.map { return UINavigationController(rootViewController: $0)}
        window.rootViewController = tabBarVC
    }

}
