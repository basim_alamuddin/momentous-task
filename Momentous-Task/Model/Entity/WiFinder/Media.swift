//
//  Media.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import Foundation

struct Media: Decodable {
    let kind: MediaType
    let artworkUrl100: URL?
    let previewUrl: URL?
    let artistName: String?
    let trackName: String?
    let longDescription: String?
    
    /// The media title to show when previewing it.
    var title: String {
        switch kind {
        case .music, .movie:
            return trackName ?? ""
        case .tvShow:
            return artistName ?? ""
        }
    }
}

enum MediaType: String, Decodable {
    case music = "song"
    case tvShow = "tv-episode"
    case movie = "feature-movie"
    
    /// The value passed to the "media" parameter in the URL.
    var requestValue: String {
        switch self {
        case .music:
            return "music"
        case .tvShow:
            return "tvShow"
        case .movie:
            return "movie"
        }
    }
    
    /// The value as displayed on UI.
    var uiValue: String {
        switch self {
        case .music:
            return "Music"
        case .tvShow:
            return "Tv Show"
        case .movie:
            return "Movie"
        }
    }
}
