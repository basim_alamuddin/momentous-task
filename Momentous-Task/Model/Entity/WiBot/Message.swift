//
//  Message.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/20/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit

enum MessageType {
    case text
    case image
}

enum MessageOwner {
    case user
    case bot
}

struct Message {
    let type: MessageType
    let owner: MessageOwner
    let date: Date = Date()
    let text: String?
    let image: UIImage?
    
    init(owner: MessageOwner, text: String) {
        type = .text
        self.owner = owner
        self.text = text
        image = nil
    }
    
    init(owner: MessageOwner, image: UIImage) {
        assert(owner != .bot, "Bot can't create image message.")
        type = .image
        self.owner = owner
        text = nil
        self.image = image
    }
}
