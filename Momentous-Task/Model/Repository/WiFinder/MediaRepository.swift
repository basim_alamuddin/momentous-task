//
//  MediaRepository.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import Moya
import RxSwift

class MediaRepository {
    
    let disposeBag = DisposeBag()
    private let api: MoyaProvider<MediaAPI>
    
    init(api: MoyaProvider<MediaAPI>) {
        self.api = api
    }
    
    /// Requests media from API data source.
    func requestMedia(term: String, mediaType: MediaType) -> Single<[Media]> {        
        return api.rx.request(.requestMedia(term, mediaType))
        .filterSuccessfulStatusCodes()
        .map([Media].self, atKeyPath: "results")
    }
}
