//
//  MediaAPI.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/18/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import Moya

enum MediaAPI {
    case requestMedia(String, MediaType)
}

extension MediaAPI: TargetType {
    
    var baseURL: URL {
        return URL(string: "https://itunes.apple.com")!
    }
    
    var path: String {
        switch self {
        case .requestMedia:
            return "/search"
        }
    }
    
    var method: Method {
        return .get
    }
    
    var task: Task {
        switch self {
        case .requestMedia(let searchTerm, let mediaType):
            var parameters = ["media": mediaType.requestValue, "term": searchTerm, "limit": "60"]
            if mediaType == .music { // Exclude music video from music response.
                parameters["entity"] = "song"
            }
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        }
    }
    
    var headers: [String: String]? {
        return nil
    }
    
    var sampleData: Data {
        return "{\"resultCount\":0,\"results\":[]}".data(using: .utf8)!
    }
}
