//
//  Log.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/17/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import Foundation

/// Checks if it is debug or another (release) build. https://stackoverflow.com/a/47443568/6516499
let isDebug: Bool = {
    var isDebug = false
    // function with a side effect and Bool return value that we can pass into assert()
    func set(debug: Bool) -> Bool {
        isDebug = debug
        return isDebug
    }
    // assert:
    // "Condition is only evaluated in playgrounds and -Onone builds."
    // so isDebug is never changed to true in Release builds
    assert(set(debug: true))
    return isDebug
}()

/// A logger utility that prints in debug only.
public class Log {
    
    private init() {}
    
    public static func print(_ item: Any) {
        if isDebug {
            Swift.print(item)
        }
    }
}
