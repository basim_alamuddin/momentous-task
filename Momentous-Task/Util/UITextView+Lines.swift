//
//  UITextView+Lines.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/21/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit

extension UITextView {
    
    func numberOfLines() -> Int {
        if let fontUnwrapped = self.font {
            return Int(self.contentSize.height / fontUnwrapped.lineHeight)
        }
        
        return 0
    }
}
