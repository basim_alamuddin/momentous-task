//
//  Notification+Keyboard.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/17/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit

extension Notification {
    
    var keyboardSize: CGSize? {
        return (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
    }
    
    var keyboardAnimationDuration: Double? {
        return userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double
    }
    
}
