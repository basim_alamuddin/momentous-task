//
//  UIViewController+Alert.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/19/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Creates alert view controller and displays it on self.
    /// Only OK action is provided.
    ///
    /// - Parameters:
    ///   - title: Alert title.
    ///   - message: Alert message.
    func showAlert(title: String = "", message: String) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "OK", style: .default)
        alertVC.addAction(dismissAction)
        DispatchQueue.main.async { [weak self] in
            self?.present(alertVC, animated: true, completion: nil)
        }
    }
}
