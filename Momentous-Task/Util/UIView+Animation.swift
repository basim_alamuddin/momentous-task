//
//  UIView+Animation.swift
//  Momentous-Task
//
//  Created by Basim Alamuddin on 12/19/18.
//  Copyright © 2018 Momentous. All rights reserved.
//

import UIKit

extension UIView {
    
    func shake(for duration: TimeInterval = 0.5, withTranslation translation: CGFloat = 8) {
        let propertyAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 0.2) {
            self.transform = CGAffineTransform(translationX: translation, y: 0)
        }
        
        propertyAnimator.addAnimations({
            self.transform = CGAffineTransform(translationX: 0, y: 0)
        }, delayFactor: 0.1)
        
        propertyAnimator.startAnimation()
    }
}
